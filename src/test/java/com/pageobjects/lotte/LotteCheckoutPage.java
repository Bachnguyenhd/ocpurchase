package com.pageobjects.lotte;

import org.openqa.selenium.By;
import com.automation.base.WebPageBase;
import com.automation.core.WebApp;
import com.automation.core.WebAppManager;

public class LotteCheckoutPage extends WebPageBase {
	private static final By CHECKOUT_PAGE = By.cssSelector(".page-layout-cart-layout");
	private static final By STEP_SHIPPING_ACTIVE = By.cssSelector(".step-shipping.active");
	private static final By STEP_DELIVERY_ACTIVE = By.cssSelector(".step-delivery.active");
	
	private static final By SHIPPING_DETAIL_NEXT_BUTTON = By.cssSelector("#shipping-detail-buttons-container button[type='submit']");
	private static final By SHIPPING_METHOD_NEXT_BUTTON=By.cssSelector("#shipping-method-buttons-container button[type='submit']");
	private static final By PAYMENT_BUTTON = By.cssSelector(".payment-method-content button[type='submit']");
	
	private static final By ORDER_ID = By.cssSelector(".order-number");

	private static final By COD_BUTTON = By.cssSelector("#group-1");
	private static final By SPINNER = By.cssSelector(".spinner");
	
	public LotteCheckoutPage() throws Exception {
		super();
	}

	@Override
	protected void init(Object[] params) throws Exception {
		try {
			app = WebAppManager.getWebApp(WebApp.class);
			driver = app.getBrowserContext().getDriver();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@Override
	// check on check out page
	protected void checkOnPage() throws Exception {
		try {
			app.safeWait(CHECKOUT_PAGE);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
	
	public void clickNextOnShippingDetail() throws Exception {
		try {
			log.info("click Shipping detail next button");
			app.safeElementWaitToDisappear(SPINNER);
			app.safeWait(STEP_SHIPPING_ACTIVE);
			app.safeClick(SHIPPING_DETAIL_NEXT_BUTTON);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
	
	public void clickNextOnDeliveryDetail() throws Exception {
		try {
			log.info("wait to shipping method screen");
			app.safeElementWaitToDisappear(SPINNER);
			app.safeWait(STEP_DELIVERY_ACTIVE);
			log.info("click Shipping method next button");
			app.safeClick(SHIPPING_METHOD_NEXT_BUTTON);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	
	// click  COD method, click Payment 
	public void chooseCOD() throws Exception {
		try {
			app.waitForAllCalls();
			app.safeClick(COD_BUTTON);
			Thread.sleep(2000);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
	
	// STEP SUCCESS
	public String getOrderId() throws Exception {
		try {
			return app.safeGetText(ORDER_ID);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
}
