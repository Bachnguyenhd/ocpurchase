package com.context;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.Status;
import org.apache.log4j.Logger;

import com.automation.base.TestContextManager;
import com.automation.base.TestEnvironments;
import com.automation.core.WebAppManager;
import com.pageobjects.common.PurchaseWebApp;
import com.unity.Order;
//import cucumber.api.Scenario;
//import cucumber.api.java.After;
//import cucumber.api.java.Before;

/**
 * Class to hold Before- and AfterScenario actions
 */
public class BaseTest {
	Logger log = Logger.getLogger(this.getClass());
	
	public BaseTest() {
	}

	@Before
	public void beforeScenario(Scenario scenario) throws Throwable {
		log.info("========================");
		log.info("Starting scenario: " + scenario.getName());
		log.info("========================");
		TestContextManager.createContext(scenario);
		if (TestContextManager.getTestContext().getEnvironment() == null) {
			log.info("No environment set, DEVELOPMENT is selected by default");
			TestContextManager.getTestContext().setEnvironment(TestEnvironments.DEVELOPMENT);
		}
		PurchaseWebApp app = (PurchaseWebApp) WebAppManager.getWebApp(PurchaseWebApp.class);
		app.getBrowserContext().getDriver().manage().deleteAllCookies();
	}

	@After
	public void afterScenario(Scenario scenario) throws Throwable {
		PurchaseWebApp app = (PurchaseWebApp) WebAppManager.getWebApp(PurchaseWebApp.class);
		Order order = app.getContext().getOrder();
		try {
			//Đổi kiểu dữ liệu từ string ->status (enum)
			Status testStatus = scenario.getStatus();
			if (testStatus == Status.PASSED) {
				log.info("Test scenario passed.");
			} else if (testStatus == Status.SKIPPED){
				log.info("Test scenario [" + scenario.getName() + "] skipped.");
			} else if (testStatus == Status.FAILED) {
				log.info("Test scenario [" + scenario.getName() + "] failed.");
				WebAppManager.takeAppScreenshots(scenario.getName());
			} else {
				log.error("Invalid test status!");
			}			
		} catch (Exception e) {
			log.error(e.toString());
			e.printStackTrace();
		} finally {
			WebAppManager.quitApp();
			log.info("========================");
			log.info("Scenario ended: " + scenario.getName());
			log.info("========================");
		}
	}
}