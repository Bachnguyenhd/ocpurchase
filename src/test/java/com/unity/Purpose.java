package com.unity;

public enum Purpose {
	PURCHASE,
	VERIFY,
	CLEAN;
}
